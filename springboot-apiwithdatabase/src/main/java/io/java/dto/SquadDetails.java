package io.java.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SquadDetails {
	
	@Id
	private String name;
	private String tribe;
	private String numberOfPeople;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTribe() {
		return tribe;
	}
	public void setTribe(String tribe) {
		this.tribe = tribe;
	}
	public String getNumberOfPeople() {
		return numberOfPeople;
	}
	public void setNumberOfPeople(String numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

}
